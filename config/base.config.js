'use strict';
require('dotenv').config();

module.exports = {
    port: process.env.SERVICE_PORT || 3000,
    server: {
        name: process.env.SERVICE_NAME ||'Sentuh Network',
        version: process.env.SERVICE_VERSION || '1.0.0',
    },
    db: {
        uri: process.env.MONGODB_URI ||'mongodb://127.0.0.1:27017/pointofsale',
        name: process.env.MONGODB_DB ||'pointofsale'
    }
}