
/**
 * Route to get all Category with method GET
 */
exports.route = {
    method: 'GET',
    path: '/api/categories',
    auth: true,
    async handler(req, res, response) {
        const { Category } = ctx.server.app;
        const categories = await Category.getAllCategory({});
        response(res, "Successfully to get all Category data", categories);        
    }
}