
/**
 * Route to update Category using method PUT
 */
exports.route = {
    method: 'PUT',
    path: '/api/category/:_id',
    auth: true,
    async handler(req, res, response){
        const { server } = ctx;
        const { Category } = server.app;
        const entity = req.body;
        const categoryId = req.params._id;
        const category = await Category.updateCategory(categoryId, entity);
        response(res, "Successfully to update category", category, "Couldn't find category by category _id: " + categoryId);
    }
}