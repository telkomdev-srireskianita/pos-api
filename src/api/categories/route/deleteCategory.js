
/**
 * Route to delete category with method DEL 
 */
exports.route = {
    method: 'DEL',
    path: '/api/category/:_id',
    auth: true,
    async handler(req, res, response) {
        const { Category } = ctx.server.app;
        const categoryId = req.params._id;
        const category = await Category.deleteCategory(categoryId);
        response(res, "Successfully to delete category", category, 'Category not found with category _id' + categoryId);        
    }
}