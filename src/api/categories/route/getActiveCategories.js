
/**
 * Route to get all Category active with method GET
 */
exports.route = {
    method: 'GET',
    path: '/api/categoriesActive',
    auth: true,
    async handler(req, res, response){
        const { Category } = ctx.server.app;
        const categories = await Category.getAllActiveCategory({});
        response(res, "Successfully to get all active categories", categories);    
    }
}