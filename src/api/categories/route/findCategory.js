
/**
 * Route to find Category using method POST
 */
exports.route = {
    method: 'POST',
    path: '/api/findcategory',
    auth: true,
    async handler(req, res, response){
        const { Category } = ctx.server.app;
        const keyword = req.body;
        const categories = await Category.findCategory(keyword.name);
        response(res, "Successfully find categories by keyword", categories)
    }
}