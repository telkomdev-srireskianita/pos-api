const getAllCategories = require('./getAllCategories');
const getCategoryById = require('./getCategoryById');
const getActiveCategories = require('./getActiveCategories');
const createCategory = require('./createCategory');
const deleteCategory = require('./deleteCategory');
const updateCategory = require('./updateCategory');
const findCategory = require('./findCategory');

module.exports = {
    getAllCategories,
    getCategoryById,
    getActiveCategories, 
    createCategory,
    deleteCategory,
    updateCategory,
    findCategory
}