
/**
 * Route to get one Category using method GET
 */
exports.route = {
    method: 'GET',
    path: '/api/category/:_id',
    auth: true,
    async handler(req, res, response) {
        const { Category } = ctx.server.app;
        const categoryId = req.params._id;
        const category = await Category.getOneCategory(categoryId);
        response(res, "Successfully Get Category", category, "Couldn't find category by category _id: " + categoryId); 
    }
}