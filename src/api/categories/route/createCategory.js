
/**
 * Route to create category with method POST
 */
exports.route = {
    method: 'POST',
    path: '/api/category',
    auth: true,
    async handler(req, res, response) {
        const { Category } = ctx.server.app;
        const entity = req.body;
        const category = await Category.createCategory(entity);
        response(res, "Succressfully to create a category", category);    
    }
}