import Mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

/*
 * Defining category schema
 * @type {Mongoose.Schema}
 */
const CategorySchema = new Mongoose.Schema({
    initial: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    active: {
        type: Number,
        default: 1
    }
});


CategorySchema.plugin(timestamps);

/**
 * Creating category collection
 */
const Category = Mongoose.model('Categories', CategorySchema);

/**
 * Exporting module
 */
export default Category;