/**
 * Create category
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function createCategory(entity) {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.create(entity).then((created) => {
        return created;
    }).catch((errCreated) => {
        throw errCreated;
    });
}

/**
 * Get all category with query = {}
 * 
 * @param {*} query 
 * @returns {Promise}
 */
async function getAllCategory(query) {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.find(query).lean().then((categories) => {
        return categories;
    }).catch((errCategories) => {
        throw errCategories;
    });
}

/**
 * Get Category with category _id
 * 
 * @param {*} categoryId 
 * @returns {Promise}
 */
async function getOneCategory(categoryId) {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.findById(categoryId).lean().then((category) => {
        return category;
    }).catch((errCategory) => {
        throw errCategory;
    })
}

/**
 * Update Category with category _id
 * 
 * @param {*} categoryId 
 * @param {*} entity 
 * @returns {Promise}
 */
async function updateCategory(categoryId, entity) {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.findByIdAndUpdate(categoryId, {
        $set: entity
    }, {
            new: true
        }).then((updated) => {
            return updated;
        }).catch((errUpdate) => {
            throw errUpdate;
        })
}

/**
 * Delete Category by category _id
 * 
 * @param {*} categoryId 
 * @returns {Promise}
 */
async function deleteCategory(categoryId) {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.findByIdAndDelete(categoryId).then((removed) => {
        return removed;
    }).catch((errRemoved) => {
        throw errRemoved;
    });
}

/**
 * Get active Category
 * 
 * @returns {Promise}
 */
async function getAllActiveCategory() {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.find({ active: 1 }).lean().then((categories) => {
        return categories;
    }).catch((errCategories) => {
        throw errCategories;
    });
}

async function findCategory(keyword) {
    const { CategoryModel } = ctx.server.app;
    return CategoryModel.aggregate([
        {
            $match: {
                $or: [
                    {
                        name: { '$regex': keyword.toLowerCase().replace(/\b\w/g, function (l) { return l.toUpperCase() }) }
                    },
                    {
                        initial: { '$regex': keyword.toUpperCase() }
                    }
                ]
            }
        }
    ]).then((category) => {
        return category;
    }).catch((errCategory) => {
        throw errCategory;
    });
}


module.exports = {
    createCategory,
    getAllCategory,
    getOneCategory,
    updateCategory,
    deleteCategory,
    getAllActiveCategory,
    findCategory
}