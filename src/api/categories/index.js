import CategoryModel from './model/model';
import route from './route';
import Category from './categories';

module.exports = {
    name: 'Categories',
    action: async () => {
        const { server } = ctx;
        Object.assign(server.app, {Category, CategoryModel})
        server.routes(route);
    }
}