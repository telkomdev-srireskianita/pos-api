

/**
 * Create product with entity
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function createProduct(entity) {
    const { ProductModel } = ctx.server.app;
    return ProductModel.create(entity).then((created) => {
        return created;
    }).catch((errCreated) => {
        throw errCreated;
    });
}

/**
 * Get all product using aggregate with categories
 * 
 * @param {*} _collection 
 * @param {*} _localField 
 * @param {*} _foreignField 
 * @param {*} _as 
 * @returns {Promise}
 */
async function getAllProduct(_collection, _localField, _foreignField, _as) {
    const { ProductModel } = ctx.server.app;
    return ProductModel.aggregate([
        {
            $lookup:
            {
                from: _collection,
                localField: _localField,
                foreignField: _foreignField,
                as: _as
            },
        }
    ]).then((product) => {
        return product;
    }).catch((errProduct) => {
        throw errProduct;
    });
}

/**
 * Get one product with product _id
 * 
 * @param {*} productId 
 * @returns {Promise}
 */
async function getOneProduct(productId) {
    const { ProductModel } = ctx.server.app;
    return ProductModel.findById(productId).lean().then((product) => {
        if (product !== null) {
            return product;
        } else {
            throw new NotFoundException("Couldn't find any product data!");
        }
    }).catch((errProduct) => {
        throw errProduct;
    })
}

/**
 * Update product with product _id and entity
 * 
 * @param {*} productId 
 * @param {*} entity 
 * @returns {Promise}
 */
async function updateProduct(productId, entity) {
    const { ProductModel } = ctx.server.app;
    return ProductModel.findByIdAndUpdate(productId, {
        $set: entity
    }, {
            new: true
        }).then((product) => {
            return product;
        }).catch((errUpdate) => {
            throw errUpdate;
        })
}

/**
 * Delete product with product _id
 * 
 * @param {*} productId 
 * @returns {Promise}
 */
async function deleteProduct(productId) {
    const { ProductModel } = ctx.server.app;
    return ProductModel.findByIdAndDelete(productId).then((removed) => {
        return removed;
    }).catch((errRemoved) => {
        throw errRemoved;
    });
}

/**
 * Find product with keyword and using aggregate with categories collection
 * 
 * @param {*} keyword 
 * @param {*} _collection 
 * @param {*} _localField 
 * @param {*} _foreignField 
 * @param {*} _as 
 * @returns {Promise}
 */
async function findProduct(keyword, _collection, _localField, _foreignField, _as) {
    const { ProductModel } = ctx.server.app;
    return ProductModel.aggregate([
        {
            $lookup:
            {
                from: _collection,
                localField: _localField,
                foreignField: _foreignField,
                as: _as
            }
        },
        {
            $match: {
                $or: [
                    {
                        name: { '$regex': keyword.toLowerCase().replace(/\b\w/g, function (l) { return l.toUpperCase() }) }
                    },
                    {
                        name: { '$regex': keyword.toUpperCase() }
                    },
                ]
            }
        }
    ]).then((product) => {
        return product;
    }).catch((errProduct) => {
        throw errProduct;
    });

}

module.exports = {
    createProduct,
    getAllProduct,
    getOneProduct,
    updateProduct,
    deleteProduct,
    findProduct
}