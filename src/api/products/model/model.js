import Mongoose, { Schema, SchemaType } from 'mongoose';
import timestamps from 'mongoose-timestamp';

/**
 * Defining product schema
 * @type {Mongoose.Schema}
 */
const ProductSchema = new Mongoose.Schema({
    initial: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    active: {
        type: Number,
        default: 1
    },
    categoryId:  Schema.Types.ObjectId
});


ProductSchema.plugin(timestamps);

/**
 * Creating product collection
 */
const Product = Mongoose.model('Products', ProductSchema);

/**
 * Exporting module
 */
export default Product;