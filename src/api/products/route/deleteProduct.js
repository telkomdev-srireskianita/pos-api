
/**
 * Route to delete product
 */
exports.route = {
    method: 'DEL',
    path: '/api/product/:_id',
    auth: true,
    async handler(req, res, response){
        const { Product } = ctx.server.app;
        const productId = req.params._id;
        const products = await Product.deleteProduct(productId);
        response(res, "Successfully delete one product", products)
    }
}