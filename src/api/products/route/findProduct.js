
/**
 * Route to find product
 */
exports.route = {
    method: 'POST',
    path: '/api/findproduct',
    auth: true,
    async handler(req, res, response){
        const { Product } = ctx.server.app;
        const keyword = req.body;
        const products = await Product.findProduct(keyword.name, 'categories', 'categoryId', '_id', 'categories');
        response(res, "Successfully find product by keyword", products)
    }
}