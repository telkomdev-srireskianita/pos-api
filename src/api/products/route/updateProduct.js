
/**
 * Route to update product
 */
exports.route = {
    method: 'PUT',
    path: '/api/product/:_id',
    auth: true,
    async handler(req, res, response){
        const { server } = ctx;
        const { Product } = server.app;
        const productId = req.params._id;
        const entity = req.body;
        const products = await Product.updateProduct(productId, entity);
        response(res, "Successfully get one product", products)
    }
}