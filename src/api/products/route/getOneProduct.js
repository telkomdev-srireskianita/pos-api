
/**
 * Route to get one product
 */
exports.route = {
    method: 'GET',
    path: '/api/product/:_id',
    auth: true,
    async handler(req, res, response){
        const { Product } = ctx.server.app;
        const productId = req.params._id;
        const products = await Product.getOneProduct(productId);
        response(res, "Successfully get one product", products)
    }
}