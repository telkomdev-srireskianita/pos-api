
/**
 * Route to create product
 */
exports.route = {
    method: 'POST',
    path: '/api/product',
    auth: true,
    async handler(req, res, response){
        const { Product } = ctx.server.app;
        const entity = req.body;
        const products = await Product.createProduct(entity);
        response(res, "Successfully get one product", products)
    }
}