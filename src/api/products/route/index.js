const createProduct = require('./createProduct');
const getAllProduct = require('./getAllProduct');
const getOneProduct = require('./getOneProduct');
const updateProduct = require('./updateProduct');
const deleteProduct = require('./deleteProduct');
const findProduct = require('./findProduct');

module.exports = {
    createProduct,
    getAllProduct,
    getOneProduct,
    updateProduct,
    deleteProduct,
    findProduct
}