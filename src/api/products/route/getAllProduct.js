
/**
 * Route to get all product
 */
exports.route = {
    method: 'GET',
    path: '/api/products',
    auth: true,
    async handler(req, res, response) {
        const { Product } = ctx.server.app;        
        const products = await Product.getAllProduct('categories', 'categoryId', '_id', 'categories');
        response(res, "Successfully get all product", products)
    }
}