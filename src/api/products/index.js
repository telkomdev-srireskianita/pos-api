import ProductModel from './model/model';
import route from './route';
import Product from './products';

module.exports = {
    name: 'Products',
    action: async () => {
        const { server } = ctx;
        Object.assign(server.app, {Product, ProductModel});
        server.routes(route);
    }
}