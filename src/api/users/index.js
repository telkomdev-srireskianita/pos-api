import UserModel from './model/model';
import route from './route';
import User from './users';

module.exports = {
    name: 'Users',
    action: async () => {
        const { server } = ctx;
        Object.assign(server.app, {User, UserModel});
        server.routes(route);
    }
}