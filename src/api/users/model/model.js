import Mongoose, { Schema, SchemaType } from 'mongoose';
import timestamps from 'mongoose-timestamp';

/**
 * Defining product schema
 * @type {Mongoose.Schema}
 */
const UserSchema = new Mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});


UserSchema.plugin(timestamps);

/**
 * Creating product collection
 */
const User = Mongoose.model('Users', UserSchema);

/**
 * Exporting module
 */
export default User;