
/**
 * Route to get one user Data
 */
exports.route = {
    method: 'PUT',
    path: '/api/user/:_id',
    auth: true,
    async handler(req, res, response){
        const { User } = ctx.server.app;
        const userId = req.params._id;
        const entity = req.body;
        const user = await User.updateUser(userId, entity);
        response(res, "Successfully to Update User data!", user, "Can't find user _id " + userId);
    }
}