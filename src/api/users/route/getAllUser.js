
/**
 * Route to get all User data
 */
exports.route = {
    method: 'GET',
    path: '/api/users',
    auth: true,
    async handler(req, res, response){
        const { User } = ctx.server.app;
        const user = await User.getAllUser({});
        response(res, "Successfully to get all User data!", user)
    }
}