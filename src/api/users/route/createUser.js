
/**
 * Route to store user data into user collection
 */
exports.route = {
    method: 'POST',
    path: '/api/user',
    auth: true,
    async handler(req, res, response) {
        const { User } = ctx.server.app;
        const entity = req.body;
        const user = await User.createUser(entity);
        response(res, "Successfully to create user data!", user)
    }
}