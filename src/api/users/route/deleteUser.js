
/**
 * Route to delete user Data
 */
exports.route = {
    method: 'DEL',
    path: '/api/user/:_id',
    auth: true,
    async handler(req, res, response){
        const { User } = ctx.server.app;
        const userId = req.params._id;
        const user = await User.deleteUser(userId);
        response(res, "Successfully to delete User data!", user, "Can't find user _id " + userId);
    }
}