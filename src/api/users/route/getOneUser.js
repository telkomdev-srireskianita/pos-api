
/**
 * Route to get one user Data
 */
exports.route = {
    method: 'GET',
    path: '/api/user/:_id',
    auth: true,
    async handler(req, res, response){
        const { User } = ctx.server.app;        
        const entity = {_id: req.params._id};
        const user = await User.getOneUser(entity);
        response(res, "Successfully to get User data!", user)
    }
}