
/**
 * Route for User Login
 */
exports.route = {
    method: 'POST',
    path: '/api/login',
    auth: false,
    async handler(req, res, response){
        const { User } = ctx.server.app;
        const entity = req.body;
        const user = await User.loginUser(entity);
        response(res, user ? "Authorized" : "Can't find user data!", user)
    }
}