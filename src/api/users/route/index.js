const createUser = require('./createUser');
const getAllUser = require('./getAllUser');
const getOneUser = require('./getOneUser');
const updateUser = require('./updateUser');
const deleteUser = require('./deleteUser');
const userLogin = require('./loginUser');

module.exports = {
    createUser, 
    getAllUser,
    getOneUser,
    updateUser,
    deleteUser,
    userLogin
}