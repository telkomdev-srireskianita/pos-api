
const jwt = require('jsonwebtoken');

/**
 * Create user data with or using POST method
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function createUser(entity) {
    const { UserModel } = ctx.server.app;
    return UserModel.create(entity).then((created) => {
        return created;
    }).catch((errCreated) => {
        throw errCreated;
    });
}

/**
 * Get all User data with entity = {} using GET method
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function getAllUser(entity){
    const { UserModel } = ctx.server.app;
    return UserModel.find(entity).then((users) => {
        return users;
    }).catch((errUsers) => {
        throw errUsers;
    });
}

/**
 * Get one User data with entity = { _id: userId } using GET method
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function getOneUser(entity) {
    const { UserModel } = ctx.server.app;
    return UserModel.find(entity).then((user) => {
        return user[0];
    }).catch((errUser) => {
        throw errUser;
    })
}

/**
 * Update User data with userId and entity using PUT method
 * 
 * @param {*} userId 
 * @param {*} entity 
 * @returns {Promise}
 */
async function updateUser(userId, entity) {
    const { UserModel } = ctx.server.app;
    return UserModel.findByIdAndUpdate(userId, {
        $set: entity
    }, {
        new: true
    }).then((user) => {
        return user;
    }).catch((errUser) => {
        throw errUser;
    })

}

/**
 * Delete one User Data with or using DEL method
 * 
 * @param {*} userId
 * @returns {Promise} 
 */
async function deleteUser(userId) {
    const { UserModel } = ctx.server.app;
    return UserModel.findByIdAndDelete(userId).then((removed) => {
        return removed;
    }).catch((errRemoved) => {
        throw errRemoved;
    });

}

/**
 * Login user with or using POST method
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function loginUser(entity) {
    const { UserModel } = ctx.server.app;
    const User = await UserModel.find(entity).select("-password").then(user => user[0] );
    if (User) {
        var token = jwt.sign({ User }, 'secretkey', { expiresIn: '30s' });
        return {
            user: User,
            accessToken: token
        }
    } else {
        return '';
    }
}

module.exports = {
    createUser,
    getAllUser,
    getOneUser,
    updateUser,
    deleteUser,
    loginUser
}