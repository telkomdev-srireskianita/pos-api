
/**
 * Route to get all order using method GET
 */
exports.route = {
    method: 'GET',
    path: '/api/orders',
    auth: true,
    async handler(req, res, response) {
        const { Order } = ctx.server.app;
        const orders = await Order.getAllOrder({});
        response(res, "Successfully to get all Order data", orders);        
    }
}