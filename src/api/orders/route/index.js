const createOrder = require('./createOrder');
const getAllOrder = require('./getAllOrder');

module.exports = {
    createOrder,
    getAllOrder,
}