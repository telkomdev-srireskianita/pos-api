
/**
 * Route to create order with method POST 
 */

exports.route = {
    method: 'POST',
    path: '/api/order',
    auth: true,
    async handler(req, res, response) {
        const { Order } = ctx.server.app;
        let entity = req.body;
        const orders = await Order.createOrder(entity);
        response(res, "Successfully to create order", orders);        
    }
}