
const GenerateCode = require('../../Helper/Helper.GenerateCode');

/**
 * Create order
 * 
 * @param {*} entity 
 * @returns {Promise}
 */
async function createOrder(entity) {
    const { OrderModel, Order } = ctx.server.app;
    const lastOrder = await Order.getLastOrder();
    entity['ref'] = GenerateCode.baseCode(lastOrder);
    return OrderModel.create(entity).then((created) => {
        return created;
    }).catch((errCreated) => {
        throw errCreated;
    });
}

/**
 * Get all order
 * 
 * @returns {Promise}
 */
async function getAllOrder() {
    const { OrderModel } = ctx.server.app;
    return OrderModel.find({}).sort({ createdAt: -1 }).lean().then((orders) => {
        return orders
    }).catch((errOrders) => {
        throw errOrders;
    })
}


/**
 * Get last order data
 * 
 * @returns {Promise}
 */
async function getLastOrder() {
    const { OrderModel } = ctx.server.app;
    return OrderModel.find().limit(1).sort({ $natural: -1 }).lean().then((order) => {
        return order;
    }).catch((errOrder) => {
        throw errOrder;
    });
}

module.exports = {
    createOrder,
    getAllOrder,
    getLastOrder
}