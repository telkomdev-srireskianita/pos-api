import OrderModel from './model/model';
import route from './route';
import Order from './orders';

module.exports = {
    name: 'Orders',
    action: async () => {
        const { server } = ctx;
        Object.assign(server.app, {Order, OrderModel});
        server.routes(route);
    }
}