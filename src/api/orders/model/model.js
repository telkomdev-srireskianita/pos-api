import Mongoose, { Schema, SchemaType } from 'mongoose';
import timestamps from 'mongoose-timestamp';

/**
 * Defining order schema
 * @type {Mongoose.Schema}
 */
const OrderSchema = new Mongoose.Schema({
    ref: {
        type: String
    },
    total: {
        type: Number
    },
    products: {
        type: Array
    }

});


OrderSchema.plugin(timestamps);

/**
 * Creating order collection
 */
const Order = Mongoose.model('Orders', OrderSchema);

/**
 * Exporting module
 */
export default Order;