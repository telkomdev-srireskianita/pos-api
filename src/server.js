
const restify = require('restify');
const mongoose = require('mongoose');
const corsMiddleware = require('restify-cors-middleware');
const config = require('../config/base.config');

/**
 * Create restify server
 * 
 * @type {*|Server}
 */
const server = restify.createServer({
    name: config.server.name,
    version: config.server.version
})

const cors = corsMiddleware({
    origins: ['*'],
    allowHeaders: ['X-App-Version'],
    exposeHeaders: []
});

/**
 * Only for testing
 */
require('./testing')(server);

/**
 * Configure restify parser plugins
 */
server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);

const baseDate = require('./Helper/Helper.Date');

/**
 * Set mongoose default promise
 */
mongoose.Promise = global.Promise;

/**
 * Connecting to DB
 */
mongoose.connect(config.db.uri, (err) => {
    if (err) {
        console.log('An error occurred while connecting to DB!');
        throw new Error(err);
    }
});
/**
 * Server listen
 */
server.listen(config.port, () => {

    // Create global ctx
    global.ctx = Object.assign({baseDate, server});

    // create global __basedir
    global.__baseDir = __dirname;
    
    // Gel server helpers
    require('./Helper/Server.Register')(server);

    // Get All Router
    require('./Helper/Helper.Route');
})