let dateHelper = require('../Helper/Helper.Date');

function generateCode() {
    this.baseCode = function (entity) {
        let code = entity[0].ref;
        let tanggal = dateHelper.setTanggal('yymm');

        if (!code) {
            code = 'SLS' + '-' + tanggal + '-' + '0000';
        }

        let number = code.slice(code.lastIndexOf('-') + 1);
        let numberPlus = parseInt(number) + 1;
        let numberPlusLength = numberPlus.toString().length;
        let genCode = number.slice(0, number.toString.length - numberPlusLength) + numberPlus;

        let result = 'SLS-'+ tanggal +'-' + genCode;
        
        return result;
    }
}

module.exports = new generateCode;