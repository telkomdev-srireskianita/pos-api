const fs = require('fs'),
    startRoute = Symbol('startRoute'),
    getApi = Symbol('getApi'),
    setApi = Symbol('setApi'),
    baseApiDir = Symbol('baseApiDir'),
    apiURLReplace = Symbol('apiURLReplace');

class Route {

    /**
     * Route constructor
     */
    constructor() {
        this[startRoute]();
    }

    /**
     * Only for start the api
     */
    [startRoute]() {
        this[setApi]();
    }

    /**
     * Get all API directory in api
     * 
     * - categories
     * - orders
     * - products
     * 
     * @returns {Array}
     */
   [getApi]() {
        return fs.readdirSync(this[baseApiDir]());
    }

    /**
     * Set api in action
     * 
     * const name = require('./src/api/name');
     * name.action(); // to run this api
     */
    [setApi]() {
        this[getApi]().map(api => {
            require(this[apiURLReplace](this[baseApiDir]() + api)).action();
        })
    }

    [apiURLReplace](url) {
        return url.split("/").join("\\");
    }

    /**
     * Set base dir for API
     * 
     * /src/api
     */
    [baseApiDir]() {
        return __baseDir + '/api/';
    }


}

module.exports = new Route();