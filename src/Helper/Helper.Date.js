
function dateHelper() {
    this.setTanggal = function (format) {
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var yy = (today.getFullYear() + "").slice(2);
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        if (format == 'dd-mm-yyyy') {
            today = dd + '-' + mm + '-' + yyyy; // 10-12-2018
        } else if (format == 'dd/mm/yyyy') {
            today = dd + '/' + mm + '/' + yyyy; // 10/12/2018
        } else if (format == 'yy/mm') {
            today = yyyy + '/' + mm + '/' + dd; // 2018/12/10
        } else if (format == 'dd mm yyyy') {
            today = dd + ' ' + mm + ' ' + yyyy; // 10 12 2018
        } else if (format == 'dd/mm/yy') {
            today = dd + '/' + mm + '/' + yyyy; // 10/12/18
        } else if (format == 'ddmmyyyy') {
            today = dd + mm + yyyy; // 10122018
        } else if (format ==='ddmmyy') {
            today = dd + mm + yy; // 101218
        } else if(format === 'yymm'){
            today = yy + mm;
        }

        return today;
    }
}

module.exports = new dateHelper();