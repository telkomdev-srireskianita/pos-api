import SuccessResponse from './Responses/SuccessResponse';
import NotFoundResponse from './Responses/NotFoundResponse';
import InternalServerErrorResponse from './Responses/InternalServerErrorResponse';
import jwt from 'jsonwebtoken';

/**
 * Route Register and Models Register
 */
module.exports = async (server) => {
    server['app'] = {};

    // server.response
    server['response'] = async (res, message, result, notFoundMessage) => {
        try {
            if (result === null) {
                NotFoundResponse(res, notFoundMessage);
            } else {
                SuccessResponse(res, message, result);
            }
        } catch (exception) {
            InternalServerErrorResponse(res, exception.message);
        }
    }

    function verifytoken(req, res, next) {
        const bearerHeader = req.headers['authorization'];

        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(' '),
                bearerToken = bearer[1];

            req.token = bearerToken;

            next();
        } else {
            res.status(401);
            res.json({
                success: false,
                error_code: 4,
                message: 'You have no authorization to access this route',
                data: null
            });
        }
    }

    // set server.routes
    server['routes'] = async (routes) => {
        try {
            for (let i in routes) {
                let route = routes[i];
                if (route.route) {
                    if (route.route.auth) {
                        server[(route.route.method).toLocaleLowerCase()](route.route.path, verifytoken, (req, res, next) => {
                            jwt.verify(req.token, 'secretkey', (err, authData) => {
                                if (err) {
                                    res.status(403);
                                    res.send({
                                        "success": false,
                                        "error_code": 4,
                                        "message": "Your session has been expired to access this page",
                                        "data": null
                                    })
                                } else {
                                    route.route.handler(req, res, server.response)
                                }
                            });
                        });
                    } else {
                        server[(route.route.method).toLocaleLowerCase()](route.route.path, (req, res, next) => {
                            route.route.handler(req, res, server.response)
                        });
                    }
                }
            };
        } catch (exception) {
            return exception;
        };

    };

    // set server.models
    server['models'] = (model) => {
        return model;
    }
}  